#!/usr/bin/env bash

echo_white() {
	echo $'\e[01;0m'"${1}"$'\e[0m'"${2}"
}

echo_gray() {
	echo $'\e[01;30m'"${1}"$'\e[0m'"${2}"
}

echo_red() {
	echo $'\e[01;31m'"${1}"$'\e[0m'"${2}"
}

echo_green() {
	echo $'\e[01;32m'"${1}"$'\e[0m'"${2}"
}

echo_yellow() {
	echo $'\e[01;33m'"${1}"$'\e[0m'"${2}"
}

echo_blue() {
	echo $'\e[01;34m'"${1}"$'\e[0m'"${2}"
}

echo_violet() {
	echo $'\e[01;35m'"${1}"$'\e[0m '"${2}"
}

echo_cyan() {
	echo $'\e[01;36m'"${1}"$'\e[0m'"${2}"
}


#Function
fn_run_docker() {
	local DOCKER=docker
	if [ -e /etc/synoinfo.conf ]; then
		DOCKER='sudo docker'
	fi

	local buildarch=${1}
	local pkgbuilddir=${2}
	local pkgdirname=${3}
	local runtype=${4}
	local makepkgargs=${5}
	local customcommand=${6}
	if [ -z "${customcommand}" ]; then
		customcommand="makepkg -cCs --noconfirm --skippgpcheck ${makepkgargs}"
	fi

	local returnvar=

	buildarch=${buildarch/any/x64}
	buildarch=${buildarch/x86_64/x64}
	buildarch=${buildarch/armv7h/armhf}
	buildarch=${buildarch/aarch64/aarch64}

	if [ -e /.dockerenv ]; then
		if [ ! -e /var/lib/pacman/sync/StoneCold.db ]; then
			pacman -Suy --noconfirm
		fi
		${customcommand}
	else
		local repository="forumi0721arch${buildarch}/arch-${buildarch}-dev"
		local dockername="arch-${buildarch}-${runtype}-${pkgdirname}"
		
		if [ -z "$(${DOCKER} images -a -q ${repository}:latest)" ]; then
			${DOCKER} pull ${repository}
		fi
		if [ ! -z "$(${DOCKER} ps -a -f "name=${dockername}" -q)" ]; then
			${DOCKER} rm -f ${dockername}
		fi

		local docker_run_alt="${TEMP_DIR}/${pkgdirname}-docker-run-alt"
		cat << EOF > "${docker_run_alt}"
#!/bin/sh

#if [ -z "\$(grep $(id -n -u) /etc/sudoers)" ]; then
#	echo "$(id -n -u) ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers
#fi

if [ -z "\$(grep StoneCold /etc/pacman.conf)" ]; then
cat << EOF2 >> /etc/pacman.conf
[StoneCold]
SigLevel = Optional TrustAll
Server = file:///stonecold-repo/\\\$arch
EOF2
fi

cd /build
pacman -Suy --noconfirm
su-exec $(id -n -u) ${customcommand}
exit \$?
EOF
		chmod 755 "${docker_run_alt}"
		if [ "${buildarch}" = "armhf" ]; then
			set -x
			${DOCKER} run $([ "${VERBOSE}" = "Y" ] && echo "-i") -t -m 2048M --rm --name ${dockername} -h arch-${buildarch}-dev-${runtype} -e RUN_USER_NAME=$(id -n -u) -e RUN_USER_UID=$(id -u) -e RUN_USER_GID=$(id -g) -e LANG=C -e LC_ALL=C -v ${BASE_DIR}/stonecold-repo:/stonecold-repo -v "${pkgbuilddir}":/build -v ${docker_run_alt}:/usr/local/bin/docker-run-alt --entrypoint=/usr/bin/qemu-arm-static ${repository} /bin/sh -c "/usr/local/bin/docker-run"
			set +x
			returnvar=$?
		elif [ "${buildarch}" = "aarch64" ]; then
			${DOCKER} run $([ "${VERBOSE}" = "Y" ] && echo "-i") -t -m 2048M --rm --name ${dockername} -h arch-${buildarch}-dev-${runtype} -e RUN_USER_NAME=$(id -n -u) -e RUN_USER_UID=$(id -u) -e RUN_USER_GID=$(id -g) -e LANG=C -e LC_ALL=C -v ${BASE_DIR}/stonecold-repo:/stonecold-repo -v "${pkgbuilddir}":/build -v ${docker_run_alt}:/usr/local/bin/docker-run-alt --entrypoint=/usr/bin/qemu-aarch64-static ${repository} /bin/sh -c "/usr/local/bin/docker-run"
			returnvar=$?
		else
			${DOCKER} run $([ "${VERBOSE}" = "Y" ] && echo "-i") -t -m 2048M --rm --name ${dockername} -h arch-${buildarch}-dev-${runtype} -e RUN_USER_NAME=$(id -n -u) -e RUN_USER_UID=$(id -u) -e RUN_USER_GID=$(id -g) -e LANG=C -e LC_ALL=C -v ${BASE_DIR}/stonecold-repo:/stonecold-repo -v "${pkgbuilddir}":/build -v ${docker_run_alt}:/usr/local/bin/docker-run-alt ${repository}
			returnvar=$?
		fi
		rm -rf "${docker_run_alt}"
	fi

	return ${returnvar}
}

fn_check_version() {
	local retvalue=0

	pushd "${1}" &>/dev/null
	local pkgfile="$(pwd)/PKGBUILD"
	local pkgdir="$(pwd)"
	local pkgdirname="$(basename "${pkgdir}")"
	popd &> /dev/null

	#Validation
	if [ -e "${pkgdir}/SOURCE" ]; then
		if [ ! -z "$(grep '^HOLDPKGVER="Y"$' ${pkgdir}/SOURCE)" ]; then
			echo_yellow " -> " "Skip (HOLDPKGVER=Y)"
			return 1 
		fi
		if [ ! -z "$(grep '^HOLDPKGVER=Y$' ${pkgdir}/SOURCE)" ]; then
			echo_yellow " -> " "Skip (HOLDPKGVER=Y)"
			return 1 
		fi
	fi

	#Get current pkgver
	local pkgver1=$(bash -c "source \"${pkgfile}\" ; echo \$([ ! -z "\${epoch}" ] && echo "\${epoch}:")\${pkgver}-\${pkgrel}")
	if [ -z "${pkgver1}" ]; then
		echo_red "Cannot get pkgver in PKGBUILD"
		return 1 
	fi

	#Current pkgver
	echo_blue " -> " "Current version : ${pkgver1}"

	#Import setting
	echo_blue " -> " "Import setting..."

	local sourcetype=
	local sourcepath=
	local pkgveropt=
	local buildopt=

	unset SOURCETYPE
	unset SOURCEPATH
	unset PKGVEROPT
	unset BUILDOPT
	unset -f GetSourcePatch
	unset -f CheckUpdate

	if [ -e "${pkgdir}/SOURCE" ]; then
		L_ENV_DISABLE_PROMPT=1 source "${pkgdir}/SOURCE"

		if [ "${HOLDPKGVER}" = "Y" ]; then
			echo_yellow " -> " "Skip (HOLDPKGVER=Y)"
			return 1 
		fi

		sourcetype=${SOURCETYPE}
		if [ "${sourcetype}" = "local" ]; then
			sourcepath="${pkgdir}"
		else
			sourcepath="${SOURCEPATH}"
		fi
	fi

	if [ -z "${sourcetype}" ]; then
		sourcetype="local"
	fi
	if [ -z "${sourcepath}" ]; then
		sourcepath="${pkgdir}"
	fi
	if [ ! -z "${PKGVEROPT}" ]; then
		pkgveropt=${PKGVEROPT}
	fi
	if [ ! -z "${BUILDOPT}" ]; then
		buildopt=${BUILDOPT}
	fi

	#GetSource
	echo_blue " -> " "Get source (${sourcetype})..."

	local tempdir="${TEMP_DIR}/${pkgdirname}-temp"
	rm -rf ${tempdir}
	mkdir -p ${tempdir}
	local downloadpath="$(fn_download "${pkgdir}" "${tempdir}" "${sourcetype}" "${sourcepath}" | tail -1)"
	if [ "$?" = "1" ]; then
		echo_red "Cannot find source"
		rm -rf "${tempdir}" "${downloadpath}"
		return 1
	elif [ "$?" = "2" ]; then
		echo_red "Cannot get source"
		rm -rf "${tempdir}" "${downloadpath}"
		return 1
	elif [ "$?" = "3" ]; then
		echo_red "Unknown source type"
		rm -rf "${tempdir}" "${downloadpath}"
		return 1
	fi

	if [ ! -e "${downloadpath}" ]; then
		echo_red "Cannot found download path : ${downloadpath}"
		rm -rf "${tempdir}" "${downloadpath}"
		return 1
	fi
	if [ ! -e "${downloadpath}/PKGBUILD" ]; then
		echo_red "Cannot found download path : ${downloadpath}"
		rm -rf "${tempdir}" "${downloadpath}"
		return 1
	fi

	#GetNewPkgversion
	echo_blue " -> " "Get new version..."

	pushd "${downloadpath}" &> /dev/null
	
	if [ ! -z "$(declare -f CheckUpdate)" ]; then
		echo_yellow " -> " "Check Update..."
		CheckUpdate
	fi
	if [ ! -z "$(declare -f GetSourcePatch)" ]; then
		echo_yellow " -> " "Apply patch..."
		GetSourcePatch
	fi

	#unset pkgver
	#unset -f pkgver
	#alias eval="echo"
	#L_ENV_DISABLE_PROMPT=1 source ./PKGBUILD &> /dev/null
	#unalias eval
	#if [ ! -z "$(declare -f pkgver)" ]; then
	
	local use_pkgver="$(bash -c "source ./PKGBUILD ; declare -f pkgver &> /dev/null && echo \"Y\"")"
	if [ "${use_pkgver}" = "Y" ]; then
		echo_yellow " -> " "Execute makepkg..."

		local pkgbuilddir="${TEMP_DIR}/${pkgdirname}-build"
		rm -rf "${pkgbuilddir}"
		cp -ar "${downloadpath}" "${pkgbuilddir}"

		local buildarch=
		if [ ! -z "$(grep "^arch=.*any.*$" PKGBUILD)" ]; then
			buildarch=x64
		elif [ ! -z "$(grep "^arch=.*x86_64.*$" PKGBUILD)" ]; then
			buildarch=x64
		elif [ ! -z "$(grep "^arch=.*armv7h.*$" PKGBUILD)" ]; then
			buildarch=armhf
		elif [ ! -z "$(grep "^arch=.*aarch64.*$" PKGBUILD)" ]; then
			buildarch=aarch64
		fi

		if [ -z "${buildarch}" ]; then
			echo_red "cannot found build arch"
			popd &> /dev/null
			rm -rf "${tempdir}" "${downloadpath}" "${pkgbuilddir}"
			return 1
		fi

		fn_run_docker "${buildarch}" "${pkgbuilddir}" "${pkgdirname}" "pkgver" "--nobuild $([ -z ${pkgveropt} ] && echo "--nodeps --nocheck" || echo "${pkgveropt}")"

		if [ "$?" != "0" ]; then
			echo_red "makepkg failed"
			popd &> /dev/null
			rm -rf "${tempdir}" "${downloadpath}" "${pkgbuilddir}"
			return 1
		else
			if [ -e "${pkgbuilddir}/PKGBUILD" ]; then
				cp -f "${pkgbuilddir}/PKGBUILD" "${downloadpath}/PKGBUILD"
				rm -rf "${pkgbuilddir}"
			fi
		fi
	fi
	#unset pkgver
	#unset -f pkgver

	popd &> /dev/null

	#Get new pkgver
	local pkgver2=$(bash -c "source \"${downloadpath}/PKGBUILD\" ; echo \$([ ! -z "\${epoch}" ] && echo "\${epoch}:")\${pkgver}-\${pkgrel}")
	if [ -z "${pkgver2}" ]; then
		echo_red "Cannot get pkgver in PKGBUILD"
		rm -rf "${tempdir}" "${TEMP_DIR}/makepkg-repo-${USER}"
		return 1 
	fi

	#Current pkgver
	echo_blue " -> " "New version : ${pkgver2}"

	#Check
	echo_blue " -> " "Compare version..."
	if [ "$(cat "${downloadpath}/PKGBUILD" | wc -l)" = "0" ]; then
		retvalue=1
		echo_red " -> " "Empty PKGBUILD file"
		exit
	else
		diff "${pkgfile}" "${downloadpath}/PKGBUILD" &> /dev/null
		if [ "${?}" = "0" ]; then
			echo_blue " -> " "Already up-to-date."
			retvalue=0
			pushd "${pkgdir}" &> /dev/null
			for buildarch in any x86_64 armv7h aarch64
			do
				if [ ! -z "$(grep "^arch=.*${buildarch}.*$" PKGBUILD)" ]; then
					if [ -z "$(ls *-${pkgver2}-${buildarch}.pkg.tar.xz 2>/dev/null)" ]; then
						echo_red " -> " "But cannot found pkg"
						retvalue=2
						break
					fi
				fi
			done
			popd &> /dev/null
		else
			retvalue=2
		fi
	fi

	if [ "${retvalue}" = "2" ]; then
		echo_cyan " -> " "Current version : ${pkgver1}"
		echo_cyan " -> " "New version : ${pkgver2}"
		echo_yellow " -> " "Update..."
		pushd "${pkgdir}" &> /dev/null
		local f=
		for f in $(ls -a --ignore=. --ignore=.. --ignore=*.pkg.tar.* --ignore=*.src.tar.* --ignore=*.bin.tar.* --ignore=SOURCE --ignore=.git)
		do
			rm -rf ${f}
		done
		popd &> /dev/null
		pushd "${pkgdir}" &> /dev/null
		local f=
		for f in $(ls -a --ignore=. --ignore=.. --ignore=*.pkg.tar.* --ignore=*.src.tar.* --ignore=*.bin.tar.* --ignore=SOURCE --ignore=.svn --ignore=.git "${downloadpath}")
		do
			cp -ar "${downloadpath}/${f}" ./
		done
		popd &> /dev/null
	fi

	#Cleanup
	echo_blue " -> " "Cleanup..."
	rm -rf "${tempdir}" "${downloadpath}" "${pkgbuilddir}"
	unset SOURCETYPE
	unset SOURCEPATH
	unset -f GetSourcePatch

	if [ "${SKIP_BUILD}" = "Y" ]; then
		return ${retvalue}
	fi

	#Build
	if [ "${retvalue}" = "2" -o "${FORCE_BUILD}" = "Y" ]; then
		echo_blue " -> " "Build..."
		local realpath="$(realpath "${1}")"
		local temppath="${TEMP_DIR}/${pkgdirname}-makepkg"
		local success=Y
		rm -rf "${temppath}"
		cp -r "${realpath}" "${temppath}"
		rm -rf "${temppath}"/*.pkg.tar.xz
		pushd "${temppath}" &>/dev/null

		for buildarch in any x86_64 armv7h aarch64
		do
			if [ ! -z "$(grep "^arch=.*${buildarch}.*$" PKGBUILD)" ]; then
				fn_run_docker "${buildarch}" "${temppath}" "${pkgdirname}" "makepkg" "${buildopt}"

				if ls "${temppath}"/*-${buildarch}.pkg.tar.xz &> /dev/null ; then
					rm -rf "${realpath}"/*-${buildarch}.pkg.tar.xz
					cp -r "${temppath}"/*-${buildarch}.pkg.tar.xz ${realpath}/
					cp -f "${temppath}"/PKGBUILD ${realpath}/
				else
					success=N
				fi
			fi
		done
		popd &>/dev/null

		if [ "${success}" = "Y" ]; then
			pushd "${realpath}" &>/dev/null
			if [ "${realpath/arch_aur_PKGBUILDs/}" != "${realpath}" -o "${realpath/arch_stonecold_PKGBUILDs/}" != "${realpath}" -o "${realpath/arch_pi_PKGBUILDs/}" != "${realpath}" -o "${realpath/arch_meta_PKGBUILDs/}" != "${realpath}" ]; then
				for buildarch in any x86_64 armv7h aarch64
				do
					if [ ! -z "$(grep "^arch=.*${buildarch}.*$" PKGBUILD)" ]; then
						fn_run_docker "${buildarch}" "${realpath}" "$(basename "${realpath}")" "srcinfo" "" "makepkg --printsrcinfo > .SRCINFO"
						break
					fi
				done
			fi
			git add .
			git commit -m "$(basename "${realpath}"): Update (Auto)"
			#git push
			popd &>/dev/null
		else
			echo_red " -> " "Build Failed"
		fi
	fi

	return ${retvalue}
}

#Function
fn_download() {
	local retvalue=
	local pkgdir="${1}"
	local tempdir="${2}"
	local sourcetype="${3}"
	local sourcepath="${4}"
	local sourcebase=
	if [ "${sourcetype}" = "AUR4" ]; then
		sourcebase="$(basename "${pkgdir}")"
	elif [ "${sourcetype}" = "ASP" ]; then
		sourcebase="${sourcepath}"
	else
		sourcebase="$(basename "${sourcepath}")"
	fi

	if [ "${sourcetype}" = "ABS" ]; then
		if [ ! -e "${sourcepath}" ]; then
			#echo "Cannot find source"
			return 1
		fi
		cp -r "${sourcepath}" "${tempdir}/"
		pushd "${tempdir}" &> /dev/null
		retvalue=$(ls -d */ | cut -d '/' -f 1)
		popd &> /dev/null
	elif [ "${sourcetype}" = "RSYNC" ]; then
		pushd "${tempdir}" &> /dev/null
		local cnt=
		for cnt in {1..10}
		do
			rsync -mrt "${sourcepath}"/* "${sourcebase}" &> /dev/null
			if [ "$?" = "0" ]; then
				break;
			fi
			rm -rf "${tempdir}/${sourcebase}"
		done
		if [ ! -e "${tempdir}/${sourcebase}" ]; then
			#echo "Cannot get source"
			popd &> /dev/null
			return 2
		fi
		retvalue="${sourcebase}"
		popd &> /dev/null
	elif [ "${sourcetype}" = "ASP" ]; then
		pushd "${tempdir}" &> /dev/null
		local cnt=
		for cnt in {1..10}
		do
			#asp export "${sourcepath}" &> /dev/null
			echo fn_run_docker "x64" "${tempdir}" "$(basename "${tempdir}")" "asp" "" "asp export ${sourcepath}"
			fn_run_docker "x64" "${tempdir}" "$(basename "${tempdir}")" "asp" "" "asp export ${sourcepath}"

			if [ -e "${tempdir}/${sourcebase}" ]; then
				break;
			fi
		done
		if [ ! -e "${tempdir}/${sourcebase}" ]; then
			#echo "Cannot get source"
			popd &> /dev/null
			return 2
		fi
		retvalue="${sourcebase}"
		popd &> /dev/null
	elif [ "${sourcetype}" = "AUR" ]; then
		local cnt=
		for cnt in {1..10}
		do
			wget "${sourcepath}" -O "${tempdir}/${sourcebase}" &>/dev/null
			if [ "$?" = "0" ]; then
				break;
			fi
			rm -rf "${tempdir}/${sourcebase}"
		done
		if [ ! -e "${tempdir}/${sourcebase}" ]; then
			#echo "Cannot get source"
			return 2
		fi
		pushd "${tempdir}" &> /dev/null
		bsdtar -xf "${tempdir}/${sourcebase}"
		rm -rf "${tempdir}/${sourcebase}"
		retvalue=$(ls -d */ | cut -d '/' -f 1)
		popd &> /dev/null
	elif [ "${sourcetype}" = "AUR4" ]; then
		pushd "${tempdir}" &> /dev/null
		local src_name="$(echo "${sourcepath}" | sed -e "s@^https://aur.archlinux.org/\(.*\).git\$@\1@g")"
		local cnt=
		for cnt in {1..10}
		do
			#git clone --depth=1 "${sourcepath}" "${tempdir}/${sourcebase}" &> /dev/null
			wget --no-check-certificate -q "https://aur.archlinux.org/cgit/aur.git/snapshot/${src_name}.tar.gz"
			if [ "$?" = "0" ]; then
				tar zxf "${src_name}.tar.gz"
				rm -rf "${src_name}.tar.gz"
				if [ "${src_name}" != "${sourcebase}" ]; then
					mv "${src_name}" "${sourcebase}"
				fi
				break;
			fi
			rm -rf "${tempdir}/${sourcebase}"
		done
		if [ ! -e "${tempdir}/${sourcebase}" ]; then
			#echo "Cannot get source"
			return 2
		fi
		retvalue=$(ls -d */ | cut -d '/' -f 1)
		popd &> /dev/null
	elif [ "${sourcetype}" = "SVN" ]; then
		pushd "${tempdir}" &> /dev/null
		local cnt=
		for cnt in {1..10}
		do
			svn co "${sourcepath}" "${sourcebase}" &> /dev/null
			if [ "$?" = "0" ]; then
				break;
			fi
			rm -rf "${tempdir}/${sourcebase}"
		done
		if [ ! -e "${tempdir}/${sourcebase}" ]; then
			#echo "Cannot get source"
			popd &> /dev/null
			return 2
		fi
		retvalue="${sourcebase}"
		popd &> /dev/null
	elif [ "${sourcetype}" = "GIT" ]; then
		pushd "${tempdir}" &> /dev/null
		local cnt=
		for cnt in {1..10}
		do
			git clone --depth=1 "${sourcepath}" "${sourcebase}"
			if [ "$?" = "0" ]; then
				break;
			fi
		done
		if [ ! -e "${sourcebase}" ]; then
			#echo "Cannot get source"
			popd &> /dev/null
			return 2
		fi
		retvalue="${sourcebase}"
		popd &> /dev/null
	elif [ "${sourcetype}" = "GITHUB_RAW" ]; then
		pushd "${tempdir}" &> /dev/null
		local url_h=$(echo ${sourcepath} | sed -e 's@https://github.com@@g' -e 's@/tree/master@/blob/master@g')
		local url_d=$(echo ${sourcepath} | sed -e 's@/tree/master@/raw/master@g')
		local cnt=
		for cnt in {1..10}
		do
			mkdir -p "${tempdir}/${sourcebase}"
			for f in $(wget ${sourcepath} -q -O - | grep "<a href=\"${url_h}/" | sed -e "s@^.*<a href=\"${url_h}/\([^\"]*\)\".*@\1@g")
			do
				wget ${url_d}/${f} -q -O ${sourcebase}/${f} &>/dev/null
			done
			if [ "$?" = "0" ]; then
				break;
			fi
			rm -rf "${tempdir}/${sourcebase}"
		done
		if [ ! -e "${sourcebase}" ]; then
			#echo "Cannot get source"
			popd &> /dev/null
			return 2
		fi
		retvalue="${sourcebase}"
		popd &> /dev/null
	elif [ "${sourcetype}" = "GIT_SUBDIR" ]; then
		pushd "${tempdir}" &> /dev/null
		local url=$(echo ${sourcepath} | cut -d '|' -f 1)
		local subdir=$(echo ${sourcepath} | cut -d '|' -f 2)
		if [ ! -z "${subdir}" ]; then
			sourcebase="${subdir}"
		fi
		sourcepath="${url}"
		local cnt=
		for cnt in {1..10}
		do
			wget ${sourcepath} -O - | tar zxvf - ${subdir}/ -C ./ &>/dev/null
			if [ "$?" = "0" ]; then
				break;
			fi
		done
		if [ ! -e "${sourcebase}" ]; then
			#echo "Cannot get source"
			popd &> /dev/null
			return 2
		fi
		retvalue="${sourcebase}"
		popd &> /dev/null
	elif [ "${sourcetype}" = "local" ]; then
		if [ ! -e "${sourcepath}" ]; then
			#echo "Cannot find source"
			return 1
		fi
		mkdir -p "${tempdir}/${sourcebase}"
		pushd "${sourcepath}" &> /dev/null
		local f=
		for f in $(ls -a --ignore=. --ignore=.. --ignore=*.pkg.tar.* --ignore=SOURCE)
		do
			cp -ar "${f}" "${tempdir}/${sourcebase}/"
		done
		popd &> /dev/null
		retvalue="${sourcebase}"
	else
		#echo "Unknown source type"
		return 3
	fi

	echo
	echo "${tempdir}/${retvalue}"

	return 0
}



#Main
if [ -e /.dockerenv -a ! -e /etc/arch-release ]; then
	echo_red "Cannot run in dockerenv"
	exit 1
fi

VERBOSE=N
SKIP_BUILD=N
FORCE_BUILD=N
BASE_DIR="$(dirname "$(realpath "${0}")")"
TEMP_DIR=${BASE_DIR}/tmp/makepkg-$(id -n -u)
rm -rf ${TEMP_DIR}
mkdir -p "${TEMP_DIR}"

while [ "${#}" -ne 0 ]
do
	case "${1}" in
		-v) VERBOSE="Y" ;;
		-s) SKIP_BUILD="Y" ;;
		-f) FORCE_BUILD="Y" ;;
	esac
	shift 1
done

for cmd in git svn
do
	if ! which ${cmd} &> /dev/null ; then
		for p in /bin /sbin /usr/bin /usr/sbin /usr/local/bin /usr/local/sbin /opt/bin /opt/sbin
		do
			if [ -e ${p}/${cmd} ]; then
				alias ${cmd}=${p}/${cmd}
				break
			fi
		done
	fi
done

UPDATE_LIST=
for src in $(find . -name PKGBUILD -not \( -path "./arch_meta_PKGBUILDs/*" -o -path "./arch_deprecated_PKGBUILDs/*" -o -path "./tmp/*" \) | sort)
do
	srcdir="$(dirname "${src}")"

	echo_green "==> " "Start - ${srcdir}"

	fn_check_version "${srcdir}"
	if [ "$?" = "2" ]; then
		UPDATE_LIST+=("${srcdir}")
	fi

	echo_green "==> " "Done."
	echo
done

if [ ${#UPDATE_LIST[@]} -gt 1 ]; then
	echo
	echo
	echo_green "==> " "Update List"
	for update in ${UPDATE_LIST[@]}
	do
		if [ ! -z "${update}" ]; then
			echo_cyan " -> " "${update}"
		fi
	done
	echo_green "==> " "Done."
	echo
fi

if [ "${SKIP_BUILD}" = "N" ]; then
	if [ -e "$(basename "${0}")" -a -e stonecold-repo ]; then
		git submodule foreach git push
		git add .
		git commit -m 'Update (Auto)'
		git push
		if [ -x ./stonecold-repo-gen.sh ]; then
			./stonecold-repo-gen.sh
		fi
	fi
fi

chmod -R +rw "${TEMP_DIR}"
rm -rf "${TEMP_DIR}"

exit 0

